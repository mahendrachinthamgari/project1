function detailsOCarId33 (inventory=[], id){
    if (inventory.length === 0){
        return [];
    }if(Array.isArray(inventory) && typeof id == 'number'){
        for(let i=0; i<inventory.length; i++){
            if(inventory[i].id === id){
                return inventory[i];
            }
        }
    }else{
        return []
    }
}

module.exports = detailsOCarId33;