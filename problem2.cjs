function LastCarDetails (inventory = []){
    if(inventory.length === 0){
        return [];
    }else if (Array.isArray(inventory)) {
        return inventory[inventory.length-1];
    }else{
        return []
    }
}

module.exports = LastCarDetails;
