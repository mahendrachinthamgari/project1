function sortAlphabitacally(inventory = []){
    if(inventory.length === 0){
        return [];
    }else if(Array.isArray(inventory)){
        const listedCars = [];
        for(let i=0; i<inventory.length; i++){
            listedCars.push(inventory[i].car_model);
        }
        listedCars.sort();
        return listedCars;
    }else{
        return []
    }
}

module.exports = sortAlphabitacally