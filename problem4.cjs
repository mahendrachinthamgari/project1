function listOfCarYears(inventory = []){
    if(Array.isArray(inventory)){
        if(inventory.length === 0){
            return []
        }
        const carsYearList = [];
        for(let i=0; i<inventory.length; i++){
            carsYearList.push(inventory[i].car_year);
        }
        return carsYearList;
    }else{
        return []
    }
}

module.exports = listOfCarYears;