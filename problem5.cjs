function oldModelCars(inventory = []){
    if(Array.isArray(inventory) && inventory.length > 0){
        const olderCars = [];
        for(let i=0; i<inventory.length; i++){
            if(inventory[i].car_year< 2000){
                olderCars.push(inventory[i])
            }
        }
        return olderCars;
    }else{
        return []
    }
}

module.exports = oldModelCars;