function BMWAudi(inventory = []){
    if(Array.isArray(inventory) && inventory.length > 0){
        const BMWAudiArray = [];
        for(let i=0; i<inventory.length; i++){
            if(inventory[i].car_make === 'BMW' || inventory[i].car_make === 'Audi'){
                BMWAudiArray.push(inventory[i]);
            }
        }
        return BMWAudiArray;
    }else{
        return []
    }
}

module.exports = BMWAudi;