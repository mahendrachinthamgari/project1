const testProblem2 = require('../problem2.cjs');
const inventory = require('./cars');

const detailsOfLastcar = testProblem2(inventory);
console.log(`Lat car is a ${detailsOfLastcar.car_make} ${detailsOfLastcar.car_model}`);
